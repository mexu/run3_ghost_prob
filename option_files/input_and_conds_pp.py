###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.hlt2_muonid import make_muon_hits as make_muon_hits_hlt2
from RecoConf.hlt1_muonid import make_muon_hits as make_muon_hits_hlt1

# This is needed for Muon decoding v2
make_muon_hits_hlt2.global_bind(geometry_version=2)
make_muon_hits_hlt1.global_bind(geometry_version=2)

options.set_input_and_conds_from_testfiledb(
    'upgrade-magup-sim10-up08-digi15-up04-30000000-velo_open-digi')

options.evt_max = -1
