/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Track.h"
#include "LHCbAlgs/Consumer.h"
#include "TrackInterfaces/IGhostProbability.h"

namespace LHCb {

  /**
   *  @author Johannes Albrecht
   *  @date   2009-10-06
   */
  struct TrackAddNNGhostId : public Algorithm::Consumer<void( Tracks const& )> {
    TrackAddNNGhostId( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, {{"inputLocation", TrackLocation::Default}} ){};
    void                          operator()( Tracks const& ) const override;
    ToolHandle<IGhostProbability> m_ghostTool{this, "GhostIdTool", "UpgradeGhostId"};
  };

  DECLARE_COMPONENT_WITH_ID( TrackAddNNGhostId, "TrackAddNNGhostId" )

} // namespace LHCb

void LHCb::TrackAddNNGhostId::operator()( LHCb::Tracks const& inCont ) const {
  for ( auto& t : inCont ) m_ghostTool->execute( *t ).ignore();
}
