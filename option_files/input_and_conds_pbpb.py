###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.hlt2_muonid import make_muon_hits as make_muon_hits_hlt2
from RecoConf.hlt1_muonid import make_muon_hits as make_muon_hits_hlt1

# This is needed for Muon decoding v2
make_muon_hits_hlt2.global_bind(geometry_version=3)
make_muon_hits_hlt1.global_bind(geometry_version=3)

options.input_files = ["/afs/cern.ch/user/d/duliu/eos/Duanqing_work/service_work/PbPb/00197804_00000007_1.xdigi",
"/afs/cern.ch/user/d/duliu/eos/Duanqing_work/service_work/PbPb/00197804_00000008_1.xdigi",
"/afs/cern.ch/user/d/duliu/eos/Duanqing_work/service_work/PbPb/00197804_00000009_1.xdigi",
"/afs/cern.ch/user/d/duliu/eos/Duanqing_work/service_work/PbPb/00197804_00000010_1.xdigi",
"/afs/cern.ch/user/d/duliu/eos/Duanqing_work/service_work/PbPb/00197804_00000011_1.xdigi",]
options.evt_max = -1
options.simulation =True
options.dddb_tag = 'dddb-20230313'
options.conddb_tag = 'sim-20230626-vc-md100'
options.input_type = 'ROOT'

