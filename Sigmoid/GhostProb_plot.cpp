#define GhostProb_plot_cxx
// The class definition in GhostProb_plot.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("GhostProb_plot.C")
// Root > T->Process("GhostProb_plot.C","some options")
// Root > T->Process("GhostProb_plot.C+")
//

#include "GhostProb_plot.h"
#include <TCanvas.h>
#include <TGraph.h>
#include <TH2.h>
#include <TLegend.h>
#include <TMath.h>
#include <TStyle.h>
#include <iostream>

using namespace std;

void GhostProb_plot::Begin(TTree * /*tree*/) {
  // The Begin() function is called at the start of the query.
  // When running with PROOF Begin() is only called on the client.
  // The tree argument is deprecated (on PROOF 0 is passed).
  TString option = GetOption();
  system("mkdir -p outputs/");
  ReadParameters();
  Init_hist();

  n_events = 0;
  n_cuts   = 0;
}

void GhostProb_plot::SlaveBegin(TTree * /*tree*/) {
  // The SlaveBegin() function is called after the Begin() function.
  // When running with PROOF SlaveBegin() is called on each slave server.
  // The tree argument is deprecated (on PROOF 0 is passed).
  TString option = GetOption();
}

Bool_t GhostProb_plot::Process(Long64_t entry) {
  // The Process() function is called for each entry in the tree (or possibly
  // keyed object in the case of PROOF) to be processed. The entry argument
  // specifies which entry in the currently loaded tree is to be processed.
  // It can be passed to either GhostProb_plot::GetEntry() or TBranch::GetEntry()
  // to read either all or the required parts of the data. When processing
  // keyed objects with PROOF, the object is already loaded and is available
  // via the fObject pointer.
  //
  // This function should contain the "body" of the analysis. It can contain
  // simple or elaborate selection criteria, run algorithms on the data
  // of the event and typically fill histograms.
  //
  // The processing can be stopped by calling Abort().
  //
  // Use fStatus to set the return value of TTree::Process().
  //
  // The return value is currently not used.

  fChain->GetTree()->GetEntry(entry);
  n_events++;
  if (n_events % 1000000 == 0)
    printf("Processing %2d.0 M events!\n", (n_events / 1000000));

  // ghost prob for plot
  _gp_infor.probA = new_ghostprob;
  _gp_infor.probB = ghostprob;
  _gp_infor.probC = tracks_TRACK_CHI2NDOF;

  // fill some histogram for reference
  if (tracks_assoc == 1) {
    n_cuts++;
    _gp_sig.push_back(_gp_infor);
    _ghostprob_hists->getTH1DptrByName("probA_sig")->Fill(new_ghostprob);
    _ghostprob_hists->getTH1DptrByName("probB_sig")->Fill(ghostprob);
    _ghostprob_hists->getTH1DptrByName("probC_sig")->Fill(tracks_TRACK_CHI2NDOF);
  } else if (tracks_assoc == 0) {
    _gp_bkg.push_back(_gp_infor);
    _ghostprob_hists->getTH1DptrByName("probA_bkg")->Fill(new_ghostprob);
    _ghostprob_hists->getTH1DptrByName("probB_bkg")->Fill(ghostprob);
    _ghostprob_hists->getTH1DptrByName("probC_bkg")->Fill(tracks_TRACK_CHI2NDOF);
  } else
    cout << "tracks_assoc = " << tracks_assoc << endl;

  if (_trk_type == 0) {

    // for different track types
    char   name[200];
    string match_type = "sig";
    if (tracks_assoc < 0.5)
      match_type = "bkg";
    sprintf(name, "probA_%s_Trk%d", match_type.c_str(), tracks_TRACK_Type);
    _ghostprob_hists->getTH1DptrByName(name)->Fill(new_ghostprob);
    sprintf(name, "probB_%s_Trk%d", match_type.c_str(), tracks_TRACK_Type);
    _ghostprob_hists->getTH1DptrByName(name)->Fill(ghostprob);

    // ghost rate as a function of xx?
    // cout<<tracks_PT<<endl;
    sprintf(name, "PT_Trk%d_%s", tracks_TRACK_Type, match_type.c_str());
    _ghostprob_hists->getTH1DptrByName(name)->Fill(tracks_PT);

    if (new_ghostprob < 0.3) {
      sprintf(name, "PT_Trk%d_%sPass", tracks_TRACK_Type, match_type.c_str());
      _ghostprob_hists->getTH1DptrByName(name)->Fill(tracks_PT);
    }
  }

  return kTRUE;
}

void GhostProb_plot::SlaveTerminate() {
  // The SlaveTerminate() function is called after all entries or objects
  // have been processed. When running with PROOF SlaveTerminate() is called
  // on each slave server.
  cout << "Processed "
       << n_events << " track candidates." << endl
       << n_cuts << " selected track candidates." << endl;

  if (_MakeSBPlot) {
    int n_sig_pass_A[_NBINS] = {0};
    int n_sig_pass_B[_NBINS] = {0};
    int n_sig_pass_C[_NBINS] = {0};

    cout << "Calculate the signal efficiency ..." << endl;
    for (int i = 0; i < _gp_sig.size(); i++) {
      for (int istep = 0; istep < _NBINS; istep++) {
        if (i % 1000000 == 0 && istep == 1)
          cout << "  " << i << "  step " << istep << endl;
        float scanpoint = 1.00 - istep * 0.0025;
        if (_gp_sig[i].probA < scanpoint)
          n_sig_pass_A[istep]++;
        if (_gp_sig[i].probB < scanpoint)
          n_sig_pass_B[istep]++;
        scanpoint = 10.0 - istep * 0.025;
        if (_gp_sig[i].probC < scanpoint)
          n_sig_pass_C[istep]++;
      }
    }

    int n_bkg_pass_A[_NBINS] = {0};
    int n_bkg_pass_B[_NBINS] = {0};
    int n_bkg_pass_C[_NBINS] = {0};

    cout << "Calculate the bkg efficiency ..." << endl;
    for (int i = 0; i < _gp_bkg.size(); i++) {
      for (int istep = 0; istep < _NBINS; istep++) {
        if (i % 1000000 == 0 && istep == 1)
          cout << "  " << i << "  step " << istep << endl;
        float scanpoint = 1.00 - istep * 0.0025;
        if (_gp_bkg[i].probA < scanpoint)
          n_bkg_pass_A[istep]++;
        if (_gp_bkg[i].probB < scanpoint)
          n_bkg_pass_B[istep]++;
        scanpoint = 10.0 - istep * 0.025;
        if (_gp_bkg[i].probC < scanpoint)
          n_bkg_pass_C[istep]++;
      }
    }
    // make plots
    TH1F *h_frame = new TH1F("frame", "frame", 100, 0.0, 1.1);
    h_frame->SetAxisRange(0.0, 1.1, "Y");
    // TH1F* h_frame = new TH1F("frame", "frame", 100, 0.5, 1.1);
    // h_frame->SetAxisRange(0.5, 1.1, "Y");
    TGraphErrors *gr_A = GP_Make_Efficiency(n_sig_pass_A, n_bkg_pass_A, "A");
    gr_A->SetTitle("A");
    gr_A->SetName("GraphA");
    TGraphErrors *gr_B = GP_Make_Efficiency(n_sig_pass_B, n_bkg_pass_B, "B");
    gr_B->SetTitle("B");
    gr_B->SetName("GraphB");
    TGraphErrors *gr_C = GP_Make_Efficiency(n_sig_pass_C, n_bkg_pass_C, "C");
    gr_C->SetTitle("C");
    gr_C->SetName("GraphC");

    gr_A->SetLineColor(2);
    gr_A->SetMarkerColor(2);
    gr_B->SetLineColor(4);
    gr_B->SetMarkerColor(4);
    gr_C->SetLineColor(1);
    gr_C->SetMarkerColor(1);

    h_frame->SetXTitle("Signal efficiency");
    h_frame->SetYTitle("Ghost rejection");

    TCanvas *c1 = new TCanvas("C1", "c1", 700, 500);
    c1->SetGridx();
    c1->SetGridy();
    h_frame->Draw("");
    gr_A->Draw("P&&same");
    gr_A->SetMarkerStyle(kDot);
    gr_A->SetFillColor(kWhite);
    gr_A->SetFillStyle(0);
    gr_B->SetMarkerStyle(kDot);
    gr_B->SetFillColor(kWhite);
    gr_B->SetFillStyle(0);
    gr_B->Draw("P&&same");
    gr_C->SetMarkerStyle(5);
    gr_C->SetFillColor(kWhite);
    gr_C->SetFillStyle(0);
    gr_C->Draw("P&&same");

    TLegend *leg = new TLegend(0.2, 0.2, 0.45, 0.45);
    leg->SetLineColor(0);
    leg->SetFillColor(0);
    leg->AddEntry(gr_A, "Retrained GP", "l");
    leg->AddEntry(gr_B, "Default GP", "l");
    leg->AddEntry(gr_C, "Track #chi^{2}/dof", "p");
    leg->Draw("same");
    // c1->BuildLegend()->SetFillColor(0);
    // c1->BuildLegend()->SetLineColor(0);
    char printname[200];
    sprintf(printname, "outputs/GP_Eff_TRK%d.eps", _trk_type);
    c1->Print(printname);
    sprintf(printname, "outputs/GP_Eff_TRK%d.pdf", _trk_type);
    c1->Print(printname);
    sprintf(printname, "outputs/GP_Eff_TRK%d.cpp", _trk_type);
    c1->Print(printname);
    sprintf(printname, "outputs/GP_Eff_TRK%d.gif", _trk_type);
    c1->Print(printname);
  }

  // make efficiency plots
  if (_trk_type == 0) {
    for (int i = 0; i < 5; i++) {
      int  trk_name = const_num::NumToTRKType(i);
      char name[200];
      sprintf(name, "PT_Trk%d_", trk_name);
      const_num::Make_ratio(_ghostprob_hists->getTH1DptrByName(((string)name + "sig").c_str()),
                            _ghostprob_hists->getTH1DptrByName(((string)name + "bkg").c_str()),
                            _ghostprob_hists->getTH1DptrByName(((string)name + "Ratio").c_str()));
      const_num::Make_ratio(_ghostprob_hists->getTH1DptrByName(((string)name + "sigPass").c_str()),
                            _ghostprob_hists->getTH1DptrByName(((string)name + "bkgPass").c_str()),
                            _ghostprob_hists->getTH1DptrByName(((string)name + "CutRatio").c_str()));
    }
  } else {
    /*
        char name[200];
        sprintf(name, "PT_Trk%d_", _trk_type);
        const_num::Make_ratio(_ghostprob_hists->getTH1DptrByName(((string)name + "sig").c_str()),
                              _ghostprob_hists->getTH1DptrByName(((string)name + "bkg").c_str()),
                              _ghostprob_hists->getTH1DptrByName(((string)name + "Ratio").c_str()));
        const_num::Make_ratio(_ghostprob_hists->getTH1DptrByName(((string)name + "sigPass").c_str()),
                              _ghostprob_hists->getTH1DptrByName(((string)name + "bkgPass").c_str()),
                              _ghostprob_hists->getTH1DptrByName(((string)name + "CutRatio").c_str()));
    */
  }
}

void GhostProb_plot::Terminate() {
  // The Terminate() function is the last function to be called during
  // a query. It always runs on the client, it can be used to present
  // the results graphically or save the results to file.
  _gp_sig.clear();
  _gp_bkg.clear();
  _ghostprob_hists->saveHistograms(_outputfile.Data());
}

TGraphErrors *GhostProb_plot::GP_Make_Efficiency(int n_sig[_NBINS], int n_bkg[_NBINS], string nametype) {
  // void GhostProb_plot::GP_Make_Efficiency(int n_sig[401], int n_bkg[401], TGraphErrors* tmp_gr){
  double sig_eff[_NBINS];
  double sig_err[_NBINS];
  double bkg_eff[_NBINS];
  double bkg_err[_NBINS];
  int    n_sig_total = n_sig[0];
  int    n_bkg_total = n_bkg[0];

  for (int i = 0; i < _NBINS; i++) {
    double n_sig_tmp = n_sig[i];
    double eff       = 0.;
    double err       = 0.;
    if (n_sig_tmp > 0) {
      eff = (1. * n_sig_tmp) / (1. * n_sig_total);
      err = const_num::Efficiency_Error(1. * n_sig_tmp, 1. * n_sig_total);
    }
    sig_eff[i] = eff;
    sig_err[i] = err;
    _ghostprob_hists->getTH1DptrByName(("Eff" + nametype + "_sig").c_str())->SetBinContent(i + 1, eff);
    _ghostprob_hists->getTH1DptrByName(("Eff" + nametype + "_sig").c_str())->SetBinError(i + 1, err);
  }

  for (int i = 0; i < _NBINS; i++) {
    double n_bkg_tmp = n_bkg_total - n_bkg[i];
    double eff       = 0.;
    double err       = 0.;
    if (n_bkg_tmp > 0) {
      eff = (1. * n_bkg_tmp) / (1. * n_bkg_total);
      err = const_num::Efficiency_Error(1. * n_bkg_tmp, 1. * n_bkg_total);
    }
    bkg_eff[i] = eff;
    bkg_err[i] = err;
    _ghostprob_hists->getTH1DptrByName(("Eff" + nametype + "_bkg").c_str())->SetBinContent(i + 1, eff);
    _ghostprob_hists->getTH1DptrByName(("Eff" + nametype + "_bkg").c_str())->SetBinError(i + 1, err);
  }

  TGraphErrors *tmp_gr = new TGraphErrors(_NBINS, sig_eff, bkg_eff, sig_err, bkg_err);

  return tmp_gr;
}
