#ifndef _INCONSTNUM_
#define _INCONSTNUM_
#include <TH1.h>
#include <TMath.h>
#include <iostream>

using namespace std;

namespace const_num {
const int _NBINS = 401;

// track types
std::string _Track_type(int icategory);

// efficiency error
double Efficiency_Error(double a, double b);

// index to track type and switich back
int NumToTRKType(int i);
int TRKTypeToNum(int type);

// get the input variables for each track types
vector<string> Get_TMVA_Variables(int i);
vector<double> Get_Variable_Bin(string var);

// transform the MLP
float transform(float MLP);

// Make efficiency plots
void Make_ratio(TH1D *deno, TH1D *nume, TH1D *ratio);
} // namespace const_num

inline std::string const_num::_Track_type(int icategory) {
  std::string event_type = "";

  if (icategory == 1)
    event_type = "Velo";
  else if (icategory == 3)
    event_type = "Long";
  else if (icategory == 4)
    event_type = "Upstream";
  else if (icategory == 5)
    event_type = "Downstream";
  else if (icategory == 6)
    event_type = "Ttrack";
  else
    event_type = "";

  return event_type;
}

inline int const_num::NumToTRKType(int i) {
  int trk_tmp = 0;
  if (i == 0)
    trk_tmp = 1;
  else if (i == 1)
    trk_tmp = 3;
  else if (i == 2)
    trk_tmp = 4;
  else if (i == 3)
    trk_tmp = 5;
  else if (i == 4)
    trk_tmp = 6;
  else
    cout << "Wrong track type looping !!!" << endl;
  return trk_tmp;
}

inline int const_num::TRKTypeToNum(int i) {
  int trk_tmp = 100;
  if (i == 1)
    trk_tmp = 0;
  else if (i == 3)
    trk_tmp = 1;
  else if (i == 4)
    trk_tmp = 2;
  else if (i == 5)
    trk_tmp = 3;
  else if (i == 6)
    trk_tmp = 4;
  else
    cout << "Wrong track type !!!" << endl;
  return trk_tmp;
}

inline double const_num::Efficiency_Error(double a, double b) {
  return TMath::Sqrt((a + 1.0) * fabs(b - a + 1.0) / (b + 2.0) / (b + 2.0) / (b + 3.0));
}

inline float const_num::transform(float MLP) {
  return 0.5 * (1. + TMath::Log(1. / MLP - 1.) / TMath::Sqrt(1. + TMath::Log(1. / MLP - 1.) * TMath::Log(1. / MLP - 1.)));
}

inline void const_num::Make_ratio(TH1D *deno, TH1D *nume, TH1D *ratio) {
  for (int i = 0; i < deno->GetNbinsX() + 1; i++) {
    double n_deno = deno->GetBinContent(i);
    double n_nume = nume->GetBinContent(i);

    double eff = 0.;
    double err = 0.;

    if ((n_deno + n_nume) > 0) {
      eff = n_nume / (n_deno + n_nume);
      err = const_num::Efficiency_Error(n_nume, n_deno + n_nume);
    }
    ratio->SetBinContent(i, eff);
    ratio->SetBinError(i, err);
  }
}

inline vector<string> const_num::Get_TMVA_Variables(int i) {
  vector<string> names;
  names.clear();
  // velo tracks
  if (i == 0) { // velo -- 11, 1
    names.push_back("UpgradeGhostInfo_obsVP");
    names.push_back("UpgradeGhostInfo_FitVeloChi2");
    names.push_back("UpgradeGhostInfo_FitVeloNDoF");
    names.push_back("UpgradeGhostInfo_veloHits");
    names.push_back("UpgradeGhostInfo_utHits");
    names.push_back("TRACK_CHI2");
    names.push_back("TRACK_NDOF");
    names.push_back("TRACK_ETA");
  } else if (i == 1) { // long -- 21, 3
    names.push_back("UpgradeGhostInfo_obsVP");
    names.push_back("UpgradeGhostInfo_FitVeloChi2");
    names.push_back("UpgradeGhostInfo_FitVeloNDoF");
    names.push_back("UpgradeGhostInfo_obsFT");
    names.push_back("UpgradeGhostInfo_FitTChi2");
    names.push_back("UpgradeGhostInfo_FitTNDoF");
    names.push_back("UpgradeGhostInfo_obsUT");
    names.push_back("UpgradeGhostInfo_FitMatchChi2");
    names.push_back("UpgradeGhostInfo_UToutlier");
    names.push_back("UpgradeGhostInfo_veloHits");
    names.push_back("UpgradeGhostInfo_utHits");
    names.push_back("TRACK_CHI2");
    names.push_back("TRACK_PT");
    names.push_back("TRACK_ETA");
  } else if (i == 2) { // upstream -- 13, 4
    names.push_back("UpgradeGhostInfo_obsVP");
    names.push_back("UpgradeGhostInfo_FitVeloChi2");
    names.push_back("UpgradeGhostInfo_FitVeloNDoF");
    names.push_back("UpgradeGhostInfo_obsUT");
    names.push_back("UpgradeGhostInfo_UToutlier");
    names.push_back("UpgradeGhostInfo_veloHits");
    names.push_back("UpgradeGhostInfo_utHits");
    names.push_back("TRACK_CHI2");
    names.push_back("TRACK_NDOF");
    names.push_back("TRACK_PT");
    names.push_back("TRACK_ETA");
  } else if (i == 3) { // downstream -- 15, 5
    names.push_back("UpgradeGhostInfo_obsFT");
    names.push_back("UpgradeGhostInfo_FitTChi2");
    names.push_back("UpgradeGhostInfo_FitTNDoF");
    names.push_back("UpgradeGhostInfo_obsUT");
    names.push_back("UpgradeGhostInfo_UToutlier");
    names.push_back("UpgradeGhostInfo_veloHits");
    names.push_back("UpgradeGhostInfo_utHits");
    names.push_back("TRACK_CHI2");
    names.push_back("TRACK_PT");
    names.push_back("TRACK_ETA");
  } else if (i == 4) { // TTrack -- 14, 6
    names.push_back("UpgradeGhostInfo_obsFT");
    names.push_back("UpgradeGhostInfo_FitTChi2");
    names.push_back("UpgradeGhostInfo_FitTNDoF");
    names.push_back("UpgradeGhostInfo_veloHits");
    names.push_back("UpgradeGhostInfo_utHits");
    names.push_back("TRACK_CHI2");
    names.push_back("TRACK_NDOF");
    names.push_back("TRACK_PT");
    names.push_back("TRACK_ETA");
  } else
    cout << "Wrong track type looping !!!" << endl;

  return names;
}

inline vector<double> const_num::Get_Variable_Bin(string var) {
  vector<double> bin_infor;
  bin_infor.clear();

  if (var.find("UpgradeGhostInfo_obsVP") != string::npos) {
    bin_infor.push_back(100);
    bin_infor.push_back(0.);
    bin_infor.push_back(2500.);
  } else if (var.find("UpgradeGhostInfo_FitVeloChi2") != string::npos) {
    bin_infor.push_back(100);
    bin_infor.push_back(0.);
    bin_infor.push_back(300.);
  } else if (var.find("UpgradeGhostInfo_FitVeloNDoF") != string::npos) {
    bin_infor.push_back(50);
    bin_infor.push_back(0.);
    bin_infor.push_back(50.);
  } else if (var.find("UpgradeGhostInfo_obsFT") != string::npos) {
    bin_infor.push_back(100);
    bin_infor.push_back(0.);
    bin_infor.push_back(1000.);
  } else if (var.find("UpgradeGhostInfo_FitTChi2") != string::npos) {
    bin_infor.push_back(50);
    bin_infor.push_back(0.);
    bin_infor.push_back(500.);
  } else if (var.find("UpgradeGhostInfo_FitTNDoF") != string::npos) {
    bin_infor.push_back(50);
    bin_infor.push_back(0.);
    bin_infor.push_back(50.);
  } else if (var.find("UpgradeGhostInfo_obsUT") != string::npos) {
    bin_infor.push_back(100);
    bin_infor.push_back(0.);
    bin_infor.push_back(4000.);
  } else if (var.find("UpgradeGhostInfo_FitMatchChi2") != string::npos) {
    bin_infor.push_back(100);
    bin_infor.push_back(0.);
    bin_infor.push_back(50.);
  } else if (var.find("UpgradeGhostInfo_UToutlier") != string::npos) {
    bin_infor.push_back(25);
    bin_infor.push_back(0.);
    bin_infor.push_back(2500.);
  } else if (var.find("UpgradeGhostInfo_veloHits") != string::npos) {
    bin_infor.push_back(100);
    bin_infor.push_back(0.);
    bin_infor.push_back(5000.);
  } else if (var.find("UpgradeGhostInfo_utHits") != string::npos) {
    bin_infor.push_back(40);
    bin_infor.push_back(0.);
    bin_infor.push_back(4000.);
  } else if (var.find("TRACK_CHI2") != string::npos) {
    bin_infor.push_back(100);
    bin_infor.push_back(0.);
    bin_infor.push_back(100.);
  } else if (var.find("TRACK_NDOF") != string::npos) {
    bin_infor.push_back(50);
    bin_infor.push_back(0.);
    bin_infor.push_back(50.);
  } else if (var.find("TRACK_PT") != string::npos) {
    bin_infor.push_back(100);
    bin_infor.push_back(0.);
    bin_infor.push_back(4000.);
  } else if (var.find("TRACK_ETA") != string::npos) {
    bin_infor.push_back(35);
    bin_infor.push_back(1.5);
    bin_infor.push_back(5.0);
  }

  return bin_infor;
}

#endif
