///////////////////////////////
// Main Program
///////////////////////////////
#include "GhostProb_comp.h"
#include "GhostProb_flat.h"
#include "GhostProb_plot.h"
#include "GhostProb_tmva.h"
#include "TChain.h"
#include "TDirectory.h"
#include <fstream>
#include <iostream>
#include <stdlib.h> //atoi
#include <unistd.h> //getopt

using namespace std;

int main(int argc, char **argv) {
  // command line parameter defaults
  int nevt    = 0;
  int trktype = 0;

  TString parameterfile("parameters.rc");

  // parse command line
  int c;
  while ((c = getopt(argc, argv, "hn:n:c:t:")) != -1) {
    switch (c) {
    case 'h':
      std::cout << "Usage: " << argv[0] << std::endl
                << "         [-h] [-n <number_of_events>] " << std::endl
                << "         [-c parameter.rc] " << std::endl
                << "         [-t track-type] " << std::endl
                << "Example:" << std::endl
                << "   ./run_GhostP -c inputs/parameters.rc -t 3" << std::endl
                << std::endl;
      exit(0);
      break;
    case 'n':
      nevt = atoi(optarg);
      if (nevt < 0)
        nevt = 0;
      break;
    case 'c':
      parameterfile = TString(optarg);
      break;
    case 't':
      trktype = atoi(optarg);
      break;
    }
  }

  gROOT->ProcessLine(".x ./lhcbStyle.C");
  // get input and output files from parameters.rc
  PParameterReader parm(parameterfile.Data());
  std::string      evt_type = (string)parm.GetChar("EvtType");

  std::cout << "==========================================="
            << "\n"
            << "           Reading Files                   "
            << "\n"
            << "===========================================" << std::endl;

  TChain *ch;

  if (evt_type.find("Plot") != std::string::npos)
    ch = new TChain("tracks");
  else if (evt_type.find("TMVA") != std::string::npos || evt_type.find("Comp") != std::string::npos)
    ch = new TChain("BestLong.UpgradeGhostIdNT/tracks");
  else if (evt_type.find("Flat") != std::string::npos)
    ch = new TChain("dataset/TrainTree");

  TString         input_roottuple;
  GhostProb_plot *ghost_plot = new GhostProb_plot;
  GhostProb_flat *ghost_flat = new GhostProb_flat;
  GhostProb_tmva *ghost_tmva = new GhostProb_tmva;
  GhostProb_comp *ghost_comp = new GhostProb_comp;

  ifstream    fin;
  std::string filelist_input = (string)(parm.GetChar("InputFileList"));
  cout<<filelist_input<<endl;

  fin.open(filelist_input.c_str());
  Int_t sum = 0;

  while (fin >> input_roottuple) {

    // for flatten study, replace the XXXX to track type
    string _input_roottuple = (string)input_roottuple;
    cout<<_input_roottuple<<endl;
    if (_input_roottuple.find("XXXX") != string::npos) {
      char name_trk[20];
      sprintf(name_trk, "%d", trktype);
      string search  = "XXXX";
      string replace = (string)name_trk;

      size_t pos = 0;
      while ((pos = _input_roottuple.find("XXXX", pos)) != std::string::npos) {
        _input_roottuple.replace(pos, search.length(), replace);
        pos += _input_roottuple.length();
      }
      input_roottuple = (TString)_input_roottuple;
    }

    // open input root file(s)
    TFile *newROOTFile = new TFile(input_roottuple);
    TTree *SelEventROOTTree;
    if (evt_type.find("Plot") != std::string::npos)
      SelEventROOTTree = (TTree *)newROOTFile->Get("tracks");
    else if (evt_type.find("TMVA") != std::string::npos || evt_type.find("Comp") != std::string::npos)
      SelEventROOTTree = (TTree *)newROOTFile->Get("BestLong.UpgradeGhostIdNT/tracks");
    else if (evt_type.find("Flat") != std::string::npos)
      SelEventROOTTree = (TTree *)newROOTFile->Get("dataset/TrainTree");

    if (!SelEventROOTTree) {
      std::cout << "Can not open ROOT file: " << input_roottuple << std::endl;
    } else {
      std::cout << "File: " << input_roottuple << ", Entries = " << SelEventROOTTree->GetEntries() << std::endl;
      ch->Add(input_roottuple);
      sum++;
    }

    delete SelEventROOTTree;
    delete newROOTFile;
  }

  std::cout << "==========================================="
            << "\n"
            << "       " << sum << " file(s) is(are) merged"
            << "      "
            << "\n"
            << "===========================================" << std::endl;

  fin.close();

  std::cout << "==========================================="
            << "\n"
            << "           Analyzing  Files                   "
            << "\n"
            << "===========================================" << std::endl;

  int runevt = ch->GetEntries();
  std::cout << "Entries: " << runevt << std::endl;
  if (nevt < runevt && nevt != 0) {
    runevt = nevt;
  }

  if (evt_type.find("Plot") != std::string::npos) {
    ghost_plot->setParameterFileName(parameterfile.Data());
    ghost_plot->setTrackType(trktype);
    ch->Process(ghost_plot, "", runevt);
  } else if (evt_type.find("TMVA") != std::string::npos) {
    ghost_tmva->setParameterFileName(parameterfile.Data());
    ghost_tmva->setTrackType(trktype);
    ch->Process(ghost_tmva, "", runevt);
  } else if (evt_type.find("Comp") != std::string::npos) {
    ghost_comp->setParameterFileName(parameterfile.Data());
    ghost_comp->setTrackType(trktype);
    ch->Process(ghost_comp, "", runevt);
  } else if (evt_type.find("Flat") != std::string::npos) {
    ghost_flat->setParameterFileName(parameterfile.Data());
    ghost_flat->setTrackType(trktype);
    ch->Process(ghost_flat, "", runevt);
  }
}
