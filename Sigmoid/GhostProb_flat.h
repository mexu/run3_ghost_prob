//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Nov  4 09:25:24 2015 by ROOT version 6.04/02
// from TTree TrainTree/TrainTree
// found on file: ../next_TMVA_6_25nsLL_1d.root
//////////////////////////////////////////////////////////

#ifndef GhostProb_flat_h
#define GhostProb_flat_h

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TSelector.h>

#include <iostream>

#include "PParameterReader.hpp"
#include "const_numbers.hpp"
#include "histo_Handle.hpp"

// Header file for the classes stored in the TTree if any.
using namespace std;
using namespace const_num;

class GhostProb_flat : public TSelector {
public:
  TTree *fChain; //! pointer to the analyzed TTree or TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  Int_t   classID;
  Char_t  className[11];
  Float_t weight;
  Float_t MLP;
  Float_t prob_MLP;

  // List of branches
  TBranch *b_classID;   //!
  TBranch *b_className; //!
  TBranch *b_weight;    //!
  TBranch *b_MLP;       //!
  TBranch *b_prob_MLP;  //!

  GhostProb_flat(TTree * /*tree*/ = 0) : fChain(0) {}
  virtual ~GhostProb_flat() {}
  virtual Int_t  Version() const { return 2; }
  virtual void   Begin(TTree *tree);
  virtual void   SlaveBegin(TTree *tree);
  virtual void   Init(TTree *tree);
  virtual Bool_t Notify();
  virtual Bool_t Process(Long64_t entry);
  virtual Int_t  GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
  virtual void   SetOption(const char *option) { fOption = option; }
  virtual void   SetObject(TObject *obj) { fObject = obj; }
  virtual void   SetInputList(TList *input) { fInput = input; }
  virtual TList *GetOutputList() const { return fOutput; }
  virtual void   SlaveTerminate();
  virtual void   Terminate();
  void           setParameterFileName(const char *s) { _parameter_filename = TString(s); }
  void           setTrackType(int ttype) { _trk_type = ttype; }

private:
  TString _parameter_filename;
  int     _trk_type;
  void    ReadParameters();

  string _outputfile;
  string _outputtext;
  string _FunctionName;
  string _EvtType;
  string _varset;
  string _dataset;
  bool   _invert;
  int    _nobin;
  bool   _flatBKG;

  void Init_hist();
  int  n_events;

  vector<float> MLP_vector;
  histo_Handle *_ghostprob_hists;

  ClassDef(GhostProb_flat, 0);
};

#endif

#ifdef GhostProb_flat_cxx
void GhostProb_flat::Init(TTree *tree) {
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set branch addresses and branch pointers
  if (!tree)
    return;
  fChain = tree;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("classID", &classID, &b_classID);
  fChain->SetBranchAddress("className", className, &b_className);
  fChain->SetBranchAddress("weight", &weight, &b_weight);
  fChain->SetBranchAddress("MLP", &MLP, &b_MLP);
  fChain->SetBranchAddress("prob_MLP", &prob_MLP, &b_prob_MLP);
}

Bool_t GhostProb_flat::Notify() {
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void GhostProb_flat::ReadParameters() {
  PParameterReader parm(_parameter_filename.Data());

  _EvtType    = parm.GetChar("EvtType", "Flat");
  _varset     = parm.GetChar("VARSET", "2022");
  _outputfile = parm.GetChar("Output", "Flat");
  _dataset    = parm.GetChar("DATASET", "25nsLL");
  _invert     = parm.GetBool("INVERT", false);
  _nobin      = parm.GetInt("NOBIN", 200);
  _flatBKG    = parm.GetBool("FlatBKG", true);

  char name[200];
  sprintf(name, "%s_%d_%s_%s", _outputfile.c_str(), _trk_type, _dataset.c_str(), _varset.c_str());
  string trk_name = const_num::_Track_type(_trk_type);

  _outputfile   = (string)name + ".root";
  _outputtext   = "outputs/Flatten" + trk_name + ".C";
  _FunctionName = trk_name + "Table";

  MLP_vector.clear();
  _ghostprob_hists = new histo_Handle();
}

void GhostProb_flat::Init_hist() {
  _ghostprob_hists->AddHist1D("hist_MLP_sig", "", "MLP", "", 100, 0, 1);
  _ghostprob_hists->AddHist1D("hist_MLP_sig_invert", "", "MLP", "", 100, 0, 1);
  _ghostprob_hists->AddHist1D("hist_MLP_bkg", "", "MLP", "", 100, 0, 1);
  _ghostprob_hists->AddHist1D("hist_MLP_bkg_invert", "", "MLP", "", 100, 0, 1);
}

#endif // #ifdef GhostProb_flat_cxx
