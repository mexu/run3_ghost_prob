#define GhostProb_tmva_cxx
// The class definition in GhostProb_tmva.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("GhostProb_tmva.C")
// Root > T->Process("GhostProb_tmva.C","some options")
// Root > T->Process("GhostProb_tmva.C+")
//

#include "GhostProb_tmva.h"
#include <TH2.h>
#include <TStyle.h>
#include <sys/times.h>

void GhostProb_tmva::Begin(TTree * /*tree*/) {
  // The Begin() function is called at the start of the query.
  // When running with PROOF Begin() is only called on the client.
  // The tree argument is deprecated (on PROOF 0 is passed).
  TString option = GetOption();

  system("mkdir -p outputs/");
  system("mkdir -p dataset/");
  system("mkdir -p dataset/weights");
  system("mkdir -p dataset/weights/outputs");

  ReadParameters();
  Init_hist();

  n_events = 0;
  n_cuts   = 0;
}

void GhostProb_tmva::SlaveBegin(TTree * /*tree*/) {
  // The SlaveBegin() function is called after the Begin() function.
  // When running with PROOF SlaveBegin() is called on each slave server.
  // The tree argument is deprecated (on PROOF 0 is passed).
  TString option = GetOption();
}

Bool_t GhostProb_tmva::Process(Long64_t entry) {
  // The Process() function is called for each entry in the tree (or possibly
  // keyed object in the case of PROOF) to be processed. The entry argument
  // specifies which entry in the currently loaded tree is to be processed.
  // It can be passed to either GhostProb_tmva::GetEntry() or TBranch::GetEntry()
  // to read either all or the required parts of the data. When processing
  // keyed objects with PROOF, the object is already loaded and is available
  // via the fObject pointer.
  //
  // This function should contain the "body" of the analysis. It can contain
  // simple or elaborate selection criteria, run algorithms on the data
  // of the event and typically fill histograms.
  //
  // The processing can be stopped by calling Abort().
  //
  // Use fStatus to set the return value of TTree::Process().
  //
  // The return value is currently not used.

  fChain->GetTree()->GetEntry(entry);
  n_events++;
  if (n_events % 1000000 == 0)
    printf("Processing %2d.0 M events!\n", (n_events / 1000000));

  // fill tree for TMVA training
  if (tracks_TRACK_Type == _trk_type) {
    if (tracks_assoc > 0.5)
      _signal->Fill();
    else
      _background->Fill();
    n_cuts++;
  }

  return kTRUE;
}

void GhostProb_tmva::SlaveTerminate() {
  // The SlaveTerminate() function is called after all entries or objects
  // have been processed. When running with PROOF SlaveTerminate() is called
  // on each slave server.

  cout << "Processed "
       << n_events << " track candidates" << endl
       << n_cuts << " selected track candidates." << endl;

  // add variable to the fitting
  clock_t tBeginTime = times(NULL);
  if (_trk_type == 1 || _trk_type == 3 || _trk_type == 4) {
    // AddVariable(dataloader, _varb, "UpgradeGhostInfo_obsVP", 'F');
    AddVariable(dataloader, _varb, "UpgradeGhostInfo_FitVeloChi2", 'F');
    // AddVariable(dataloader, _varb, "UpgradeGhostInfo_FitVeloNDoF", 'F');
  }

  if (_trk_type == 3 || _trk_type == 5 || _trk_type == 6) {
    // AddVariable(dataloader, _varb, "UpgradeGhostInfo_obsFT", 'F');
    AddVariable(dataloader, _varb, "UpgradeGhostInfo_FitTChi2", 'F');
    AddVariable(dataloader, _varb, "UpgradeGhostInfo_FitTNDoF", 'F');
  }
  /*
  if (_trk_type == 3 || _trk_type == 4 || _trk_type == 5) {
    AddVariable(dataloader, _varb, "UpgradeGhostInfo_obsUT", 'F');
  }
  */
  if (_trk_type == 3) {
    AddVariable(dataloader, _varb, "UpgradeGhostInfo_FitMatchChi2", 'F');
  }
  /*
  if (_trk_type == 3 || _trk_type == 4 || _trk_type == 5) {
    AddVariable(dataloader, _varb, "UpgradeGhostInfo_UToutlier", 'F');
  }
  */
  // AddVariable(dataloader, _varb, "UpgradeGhostInfo_veloHits", 'F');
  // AddVariable(dataloader, _varb, "UpgradeGhostInfo_utHits", 'F');
  AddVariable(dataloader, _varb, "TRACK_CHI2", 'F');

  if (_trk_type == 1 || _trk_type == 4 || _trk_type == 6)
    AddVariable(dataloader, _varb, "TRACK_NDOF", 'F');

  if (_trk_type == 3 || _trk_type == 4 || _trk_type == 5 || _trk_type == 6)
    AddVariable(dataloader, _varb, "TRACK_PT", 'F');
  AddVariable(dataloader, _varb, "TRACK_NDOF", 'F');
  AddVariable(dataloader, _varb, "TRACK_ETA", 'F');

  // background and signal trees
  dataloader->AddSignalTree(_signal);
  dataloader->AddBackgroundTree(_background);

  // prepare the training and test trees
  dataloader->PrepareTrainingAndTestTree("", "", _traintest_opt);

  // TMVA book
  factory->BookMethod(dataloader, TMVA::Types::kMLP, _implemention, _bookmethod_opt);

  // Train MVAs using the set of training events
  factory->TrainAllMethods();

  // Evaluate all MVAs using the set of test events
  factory->TestAllMethods();

  // Evaluate and compare performance of all configured MVAs
  factory->EvaluateAllMethods();

  clock_t tEndTime = times(NULL);
  fCostTime        = (double)(tEndTime - tBeginTime) / sysconf(_SC_CLK_TCK);

  printf("[times] TMVA trainning Cost Time = %fSec\n", fCostTime);
}

void GhostProb_tmva::Terminate() {
  // The Terminate() function is the last function to be called during
  // a query. It always runs on the client, it can be used to present
  // the results graphically or save the results to file.
  // outputFile->Write();
  delete _signal;
  delete _background;

  _output_root->Close();
  //_ghostprob_hists->saveHistograms(_outputfile);
}

void GhostProb_tmva::AddVariable(TMVA::DataLoader *fac, std::string &vars, std::string varname, char type) {
  fac->AddVariable(varname.c_str(), type);
  // int tmp = vars.length();
  // if(vars == "A") {
  if (vars.length() != 0) {
    vars += ":";
  }
  //}else vars += ":";
  vars += varname;
  return;
}
