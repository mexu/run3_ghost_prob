//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov  3 20:21:52 2015 by ROOT version 5.34/32
// from TTree tracks/tracks
// found on file: /afs/cern.ch/user/h/hyin/storage/workspace/Skimmed_samples/GhostPorb/1.6/myalg2.root
//////////////////////////////////////////////////////////

#ifndef GhostProb_tmva_h
#define GhostProb_tmva_h

#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TROOT.h>
#include <TSelector.h>
#include <TSystem.h>
#include <TTree.h>

#include "TMVA/DataLoader.h"
#include "TMVA/Factory.h"
#include "TMVA/MethodCategory.h"
#include "TMVA/Tools.h"

#include "PParameterReader.hpp"
#include "const_numbers.hpp"
#include "histo_Handle.hpp"

#include <iostream>
#include <string>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.
using namespace std;
using namespace const_num;

class GhostProb_tmva : public TSelector {
public:
  TTree *fChain; //! pointer to the analyzed TTree or TChain

  // Declaration of leaf types
  Float_t UpgradeGhostInfo_obsVP;
  Float_t UpgradeGhostInfo_FitVeloChi2;
  Float_t UpgradeGhostInfo_FitVeloNDoF;
  Float_t UpgradeGhostInfo_obsFT;
  Float_t UpgradeGhostInfo_FitTChi2;
  Float_t UpgradeGhostInfo_FitTNDoF;
  Float_t UpgradeGhostInfo_obsUT;
  Float_t UpgradeGhostInfo_FitMatchChi2;
  Float_t UpgradeGhostInfo_UToutlier;
  Float_t UpgradeGhostInfo_veloHits;
  Float_t UpgradeGhostInfo_utHits;
  Float_t TRACK_CHI2;
  Float_t TRACK_NDOF;
  Float_t TRACK_PT;
  Float_t TRACK_ETA;
  Float_t ghostprob;
  Float_t tracks_PP_TrackHistory;
  Float_t tracks_TRACK_Type;
  Float_t tracks_assoc;
  Float_t mctruepid;

  // List of branches
  TBranch *b_UpgradeGhostInfo_obsVP;        //!
  TBranch *b_UpgradeGhostInfo_FitVeloChi2;  //!
  TBranch *b_UpgradeGhostInfo_FitVeloNDoF;  //!
  TBranch *b_UpgradeGhostInfo_obsFT;        //!
  TBranch *b_UpgradeGhostInfo_FitTChi2;     //!
  TBranch *b_UpgradeGhostInfo_FitTNDoF;     //!
  TBranch *b_UpgradeGhostInfo_obsUT;        //!
  TBranch *b_UpgradeGhostInfo_FitMatchChi2; //!
  TBranch *b_UpgradeGhostInfo_UToutlier;    //!
  TBranch *b_UpgradeGhostInfo_veloHits;     //!
  TBranch *b_UpgradeGhostInfo_utHits;       //!
  TBranch *b_TRACK_CHI2;                    //!
  TBranch *b_TRACK_NDOF;                    //!
  TBranch *b_TRACK_PT;                      //!
  TBranch *b_TRACK_ETA;                     //!
  TBranch *b_ghostprob;                     //!
  TBranch *b_tracks_PP_TrackHistory;        //!
  TBranch *b_tracks_TRACK_Type;             //!
  TBranch *b_tracks_assoc;                  //!
  TBranch *b_mctruepid;                     //!

  GhostProb_tmva(TTree * /*tree*/ = 0) : fChain(0) {}
  virtual ~GhostProb_tmva() {}
  virtual Int_t  Version() const { return 2; }
  virtual void   Begin(TTree *tree);
  virtual void   SlaveBegin(TTree *tree);
  virtual void   Init(TTree *tree);
  virtual Bool_t Notify();
  virtual Bool_t Process(Long64_t entry);
  virtual Int_t  GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
  virtual void   SetOption(const char *option) { fOption = option; }
  virtual void   SetObject(TObject *obj) { fObject = obj; }
  virtual void   SetInputList(TList *input) { fInput = input; }
  virtual TList *GetOutputList() const { return fOutput; }
  virtual void   SlaveTerminate();
  virtual void   Terminate();
  void           setParameterFileName(const char *s) { _parameter_filename = TString(s); }
  void           setTrackType(int ttype) { _trk_type = ttype; }

private:
  TString _parameter_filename;
  int     _trk_type;
  void    ReadParameters();

  string _outputfile;
  string _EvtType;
  string _varset;
  string _dataset;
  string _factory_opt;
  string _factory_opt1;
  string _traintest_opt;
  string _activation_func;
  string _bookmethod_opt;
  string _implemention;
  void   Init_hist();

  int    n_events;
  int    n_cuts;
  double fCostTime;

  // TMVA fitting
  TMVA::Factory    *factory;
  TMVA::DataLoader *dataloader;

  TFile      *_output_root;
  TTree      *_signal;
  TTree      *_background;
  std::string _varb;
  void        AddVariable(TMVA::DataLoader *fac, std::string &vars, std::string varname, char type);

  ClassDef(GhostProb_tmva, 0);
};

#endif

#ifdef GhostProb_tmva_cxx
void GhostProb_tmva::Init(TTree *tree) {
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set branch addresses and branch pointers
  if (!tree)
    return;
  fChain = tree;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("UpgradeGhostInfo_obsVP", &UpgradeGhostInfo_obsVP, &b_UpgradeGhostInfo_obsVP);
  fChain->SetBranchAddress("UpgradeGhostInfo_FitVeloChi2", &UpgradeGhostInfo_FitVeloChi2, &b_UpgradeGhostInfo_FitVeloChi2);
  fChain->SetBranchAddress("UpgradeGhostInfo_FitVeloNDoF", &UpgradeGhostInfo_FitVeloNDoF, &b_UpgradeGhostInfo_FitVeloNDoF);
  fChain->SetBranchAddress("UpgradeGhostInfo_obsFT", &UpgradeGhostInfo_obsFT, &b_UpgradeGhostInfo_obsFT);
  fChain->SetBranchAddress("UpgradeGhostInfo_FitTChi2", &UpgradeGhostInfo_FitTChi2, &b_UpgradeGhostInfo_FitTChi2);
  fChain->SetBranchAddress("UpgradeGhostInfo_FitTNDoF", &UpgradeGhostInfo_FitTNDoF, &b_UpgradeGhostInfo_FitTNDoF);
  fChain->SetBranchAddress("UpgradeGhostInfo_obsUT", &UpgradeGhostInfo_obsUT, &b_UpgradeGhostInfo_obsUT);
  fChain->SetBranchAddress("UpgradeGhostInfo_FitMatchChi2", &UpgradeGhostInfo_FitMatchChi2, &b_UpgradeGhostInfo_FitMatchChi2);
  fChain->SetBranchAddress("UpgradeGhostInfo_UToutlier", &UpgradeGhostInfo_UToutlier, &b_UpgradeGhostInfo_UToutlier);
  fChain->SetBranchAddress("UpgradeGhostInfo_veloHits", &UpgradeGhostInfo_veloHits, &b_UpgradeGhostInfo_veloHits);
  fChain->SetBranchAddress("UpgradeGhostInfo_utHits", &UpgradeGhostInfo_utHits, &b_UpgradeGhostInfo_utHits);
  fChain->SetBranchAddress("TRACK_CHI2", &TRACK_CHI2, &b_TRACK_CHI2);
  fChain->SetBranchAddress("TRACK_NDOF", &TRACK_NDOF, &b_TRACK_NDOF);
  fChain->SetBranchAddress("TRACK_PT", &TRACK_PT, &b_TRACK_PT);
  fChain->SetBranchAddress("TRACK_ETA", &TRACK_ETA, &b_TRACK_ETA);
  fChain->SetBranchAddress("ghostprob", &ghostprob, &b_ghostprob);
  fChain->SetBranchAddress("tracks_PP_TrackHistory", &tracks_PP_TrackHistory, &b_tracks_PP_TrackHistory);
  fChain->SetBranchAddress("tracks_TRACK_Type", &tracks_TRACK_Type, &b_tracks_TRACK_Type);
  fChain->SetBranchAddress("tracks_assoc", &tracks_assoc, &b_tracks_assoc);
  fChain->SetBranchAddress("mctruepid", &mctruepid, &b_mctruepid);
}

Bool_t GhostProb_tmva::Notify() {
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void GhostProb_tmva::ReadParameters() {
  PParameterReader parm(_parameter_filename.Data());

  _EvtType         = parm.GetChar("EvtType", "TMVA");
  _varset          = parm.GetChar("VARSET", "2022");
  _outputfile      = parm.GetChar("Output", "TMVA");
  _dataset         = parm.GetChar("DATASET", "25nsLL");
  _factory_opt     = parm.GetChar("FACTORYOPT", "!V:!Silent:Transformations=I");
  _factory_opt1    = parm.GetChar("FACTORYOPT1", ":!Color:!DrawProgressBar");
  _traintest_opt   = parm.GetChar("TRAINTESTOPT", "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:SplitSeed=500:NormMode=NumEvents:!V");
  _activation_func = parm.GetChar("ACTIVATION", ":NeuronType=ReLU");
  _bookmethod_opt  = parm.GetChar("BOOKOPT", "!H:!V:VarTransform=N:NCycles=600:HiddenLayers=N+5:TestRate=5:EstimatorType=CE:!UseRegulator:CreateMVAPdfs=True");
  _bookmethod_opt += _activation_func;
  _implemention = parm.GetChar("IMPLEMENTION", "MLP");

  char name[200];
  sprintf(name, "%s_%d_%s_%s", _outputfile.c_str(), _trk_type, _dataset.c_str(), _varset.c_str());
  _outputfile = (string)name + ".root";

  _output_root = new TFile(_outputfile.c_str(), "recreate");
  _signal      = new TTree("sigTree", "sigTree");
  _background  = new TTree("bkgTree", "bkgTree");

  _signal->Branch("UpgradeGhostInfo_obsVP", &UpgradeGhostInfo_obsVP, "UpgradeGhostInfo_obsVP/F");
  _signal->Branch("UpgradeGhostInfo_FitVeloChi2", &UpgradeGhostInfo_FitVeloChi2, "UpgradeGhostInfo_FitVeloChi2/F");
  _signal->Branch("UpgradeGhostInfo_FitVeloNDoF", &UpgradeGhostInfo_FitVeloNDoF, "UpgradeGhostInfo_FitVeloNDoF/F");
  _signal->Branch("UpgradeGhostInfo_obsFT", &UpgradeGhostInfo_obsFT, "UpgradeGhostInfo_obsFT/F");
  _signal->Branch("UpgradeGhostInfo_FitTChi2", &UpgradeGhostInfo_FitTChi2, "UpgradeGhostInfo_FitTChi2/F");
  _signal->Branch("UpgradeGhostInfo_FitTNDoF", &UpgradeGhostInfo_FitTNDoF, "UpgradeGhostInfo_FitTNDoF/F");
  _signal->Branch("UpgradeGhostInfo_obsUT", &UpgradeGhostInfo_obsUT, "UpgradeGhostInfo_obsUT/F");
  _signal->Branch("UpgradeGhostInfo_FitMatchChi2", &UpgradeGhostInfo_FitMatchChi2, "UpgradeGhostInfo_FitMatchChi2/F");
  _signal->Branch("UpgradeGhostInfo_UToutlier", &UpgradeGhostInfo_UToutlier, "UpgradeGhostInfo_UToutlier/F");
  _signal->Branch("UpgradeGhostInfo_veloHits", &UpgradeGhostInfo_veloHits, "UpgradeGhostInfo_veloHits/F");
  _signal->Branch("UpgradeGhostInfo_utHits", &UpgradeGhostInfo_utHits, "UpgradeGhostInfo_utHits/F");
  _signal->Branch("TRACK_CHI2", &TRACK_CHI2, "TRACK_CHI2/F");
  _signal->Branch("TRACK_NDOF", &TRACK_NDOF, "TRACK_NDOF/F");
  _signal->Branch("TRACK_PT", &TRACK_PT, "TRACK_PT/F");
  _signal->Branch("TRACK_ETA", &TRACK_ETA, "TRACK_ETA/F");
  _signal->Branch("ghostprob", &ghostprob, "ghostprob/F");
  _signal->Branch("tracks_PP_TrackHistory", &tracks_PP_TrackHistory, "tracks_PP_TrackHistory/F");
  _signal->Branch("tracks_TRACK_Type", &tracks_TRACK_Type, "tracks_TRACK_Type/F");
  _signal->Branch("tracks_assoc", &tracks_assoc, "tracks_assoc/F");
  _signal->Branch("mctruepid", &mctruepid, "mctruepid/F");

  _background->Branch("UpgradeGhostInfo_obsVP", &UpgradeGhostInfo_obsVP, "UpgradeGhostInfo_obsVP/F");
  _background->Branch("UpgradeGhostInfo_FitVeloChi2", &UpgradeGhostInfo_FitVeloChi2, "UpgradeGhostInfo_FitVeloChi2/F");
  _background->Branch("UpgradeGhostInfo_FitVeloNDoF", &UpgradeGhostInfo_FitVeloNDoF, "UpgradeGhostInfo_FitVeloNDoF/F");
  _background->Branch("UpgradeGhostInfo_obsFT", &UpgradeGhostInfo_obsFT, "UpgradeGhostInfo_obsFT/F");
  _background->Branch("UpgradeGhostInfo_FitTChi2", &UpgradeGhostInfo_FitTChi2, "UpgradeGhostInfo_FitTChi2/F");
  _background->Branch("UpgradeGhostInfo_FitTNDoF", &UpgradeGhostInfo_FitTNDoF, "UpgradeGhostInfo_FitTNDoF/F");
  _background->Branch("UpgradeGhostInfo_obsUT", &UpgradeGhostInfo_obsUT, "UpgradeGhostInfo_obsUT/F");
  _background->Branch("UpgradeGhostInfo_FitMatchChi2", &UpgradeGhostInfo_FitMatchChi2, "UpgradeGhostInfo_FitMatchChi2/F");
  _background->Branch("UpgradeGhostInfo_UToutlier", &UpgradeGhostInfo_UToutlier, "UpgradeGhostInfo_UToutlier/F");
  _background->Branch("UpgradeGhostInfo_veloHits", &UpgradeGhostInfo_veloHits, "UpgradeGhostInfo_veloHits/F");
  _background->Branch("UpgradeGhostInfo_utHits", &UpgradeGhostInfo_utHits, "UpgradeGhostInfo_utHits/F");
  _background->Branch("TRACK_CHI2", &TRACK_CHI2, "TRACK_CHI2/F");
  _background->Branch("TRACK_NDOF", &TRACK_NDOF, "TRACK_NDOF/F");
  _background->Branch("TRACK_PT", &TRACK_PT, "TRACK_PT/F");
  _background->Branch("TRACK_ETA", &TRACK_ETA, "TRACK_ETA/F");
  _background->Branch("ghostprob", &ghostprob, "ghostprob/F");
  _background->Branch("tracks_PP_TrackHistory", &tracks_PP_TrackHistory, "tracks_PP_TrackHistory/F");
  _background->Branch("tracks_TRACK_Type", &tracks_TRACK_Type, "tracks_TRACK_Type/F");
  _background->Branch("tracks_assoc", &tracks_assoc, "tracks_assoc/F");
  _background->Branch("mctruepid", &mctruepid, "mctruepid/F");

  _varb = "";

  // accessor to single instance
  TMVA::Tools::Instance();

  string factoryOptions = _factory_opt + _factory_opt1;
  factory               = new TMVA::Factory((string)name, _output_root, factoryOptions);
  dataloader            = new TMVA::DataLoader("dataset");
}

void GhostProb_tmva::Init_hist() {
  // trainning input trees
  // gROOT->cd();
  // signal     = fChain->CopyTree("tracks_assoc>0.5&&tracks_TRACK_Type==4");
  //`background = fChain->CopyTree("tracks_assoc<0.5&&tracks_TRACK_Type==4");
  // signal     = fChain->CopyTree(Form("tracks_assoc > 0.5 && tracks_TRACK_Type == %d", _trk_type));
  // background = fChain->CopyTree(Form("tracks_assoc < 0.5 && tracks_TRACK_Type == %d", _trk_type));
}

#endif // #ifdef GhostProb_tmva_cxx
