#define GhostProb_eval_cxx
// The class definition in GhostProb_eval.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("GhostProb_eval.C")
// Root > T->Process("GhostProb_eval.C","some options")
// Root > T->Process("GhostProb_eval.C+")
//

#include "GhostProb_eval.h"
#include <TH2.h>
#include <TStyle.h>
#include <sys/times.h>
#include <fstream>
//
// main function
//
int main(int argc, char **argv)
{
  // command line parameter defaults
  int nevt = 0;
  int trktype = 0;

  TString parameterfile("parameters.rc");

  // parse command line
  int c;
  while ((c = getopt(argc, argv, "hn:n:c:t:")) != -1)
  {
    switch (c)
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << std::endl
                << "         [-h] [-n <number_of_events>] " << std::endl
                << "         [-c parameter.rc] " << std::endl
                << "         [-t track-type] " << std::endl
                << "Example:" << std::endl
                << "   ./run_eval -c inputs/parameters.rc_Eval -t 3" << std::endl
                << std::endl;
      exit(0);
      break;
    case 'n':
      nevt = atoi(optarg);
      if (nevt < 0)
        nevt = 0;
      break;
    case 'c':
      parameterfile = TString(optarg);
      break;
    case 't':
      trktype = atoi(optarg);
      break;
    }
  }

  // get input and output files from parameters.rc
  PParameterReader parm(parameterfile.Data());
  std::string evt_type = (string)parm.GetChar("EvtType");

  GhostProb_eval *ghost_eval = new GhostProb_eval(parameterfile.Data(), trktype, nevt);
}

//
// GhostProb_eval main processor
//
GhostProb_eval::GhostProb_eval(const char *s, int trk_type, int nevt)
{
  system("mkdir -p outputs/");

  _run_evts = nevt;
  _trk_type = trk_type;
  _parameter_filename = (TString)s;

  cout << "  --> Read parameters ..." << endl;
  ReadParameters();
  cout << "  --> Calculate new GhostProb ..." << endl;
  ReCal_TMVA();
  cout << "  --> End of te new GhostProb ..." << endl;
  EndEvaluate();

  return;
}

//
// add variable to TMVA
//
void GhostProb_eval::GPAddVariable(TMVA::Reader *red, TTree *tree, std::string varname)
{
  float *place = new float();
  tree->SetBranchAddress(varname.c_str(), place);
  red->AddVariable(varname, place);
}

//
// read parameters
//
void GhostProb_eval::ReadParameters()
{
  PParameterReader parm(_parameter_filename.Data());

  _filelist = parm.GetChar("InputFile", "test.root");
  _inputtree = parm.GetChar("INPUTTREE", "BestLong.UpgradeGhostIdNT/tracks");
  _EvtType = parm.GetChar("EvtType", "Eval");
  _varset = parm.GetChar("VARSET", "2022");
  _outputfile = parm.GetChar("Output", "TMVA");
  _dataset = parm.GetChar("DATASET", "25nsLL");
  _reader_opt = parm.GetChar("READEROPT", "!Color:!Silent");
  _implemention = parm.GetChar("IMPLEMENTION", "MLP");

  char name[200];

  sprintf(name, "%s_Eval_%d_%s_%s.root", _outputfile.c_str(), _trk_type, _dataset.c_str(), _varset.c_str());
  _outputfile = (string)name;

  sprintf(name, "dataset/weights/outputs/TMVA_%d_%s_%s_MLP.weights.xml", _trk_type, _dataset.c_str(), _varset.c_str());
  _weight_input = (string)name;
  TString InputTxt;
  if (_trk_type == 1)
    InputTxt = "FlattenVelo.txt";
  if (_trk_type == 3)
    InputTxt = "FlattenLong.txt";
  if (_trk_type == 4)
    InputTxt = "FlattenUptream.txt";
  if (_trk_type == 5)
    InputTxt = "FlattenDownstream.txt";
  if (_trk_type == 6)
    InputTxt = "FlattenTTrack.txt";

  m_xedges.clear();
  ifstream input(InputTxt);
  while (!input.eof())
  {
    float flat_value;
    input >> flat_value;
    m_xedges.push_back(flat_value);
  }
  input.close();
  m_width = 1.f / (m_xedges.size() - 2);
  cout << m_width << endl;

  n_events = 0;
  n_cuts = 0;

  _trks_chi2dof_vec.clear();
  _trks_assoc_vec.clear();
  _trks_ghostprob_vec.clear();
  _new_ghost_vec.clear();
  _trks_type_vec.clear();
  _trks_pt_vec.clear();
  _processed_evts.clear();
}

//
// calculate the new ghost prob
//
void GhostProb_eval::ReCal_TMVA()
{
  //
  // fill some information into vectors
  //
  TFile *inFile_pre = TFile::Open(_filelist.c_str());
  TTree *thetree_pre = (TTree *)inFile_pre->Get(_inputtree.c_str());

  thetree_pre->SetBranchAddress("TRACK_CHI2", &_TRACK_CHI2);
  thetree_pre->SetBranchAddress("TRACK_NDOF", &_TRACK_NDOF);
  thetree_pre->SetBranchAddress("tracks_assoc", &_tracks_assoc);
  thetree_pre->SetBranchAddress("ghostprob", &_ghostprob);
  thetree_pre->SetBranchAddress("TRACK_PT", &_TRACKS_PT);
  thetree_pre->SetBranchAddress("tracks_TRACK_Type", &_tracks_TRACK_Type);

  int ntotal_evts = thetree_pre->GetEntries();
  if (_run_evts > 0)
    ntotal_evts = _run_evts;

  // tmva time: before implemention
  cout << "    -> Read useful information to vectors ..." << endl;
  int _index = 0;
  for (int ievt = 0; ievt < ntotal_evts; ievt++)
  {
    thetree_pre->GetEntry(ievt);

    int _tracks_assoc_ = (int)_tracks_assoc;
    int _tracks_TRACK_Type_ = (int)_tracks_TRACK_Type;
    // cout<<"_TRACK_CHI2   "<<_TRACK_CHI2<<endl;
    // cout<<"_TRACK_NDOF   "<<_TRACK_NDOF<<endl;
    Float_t chi2dof = (Float_t)_TRACK_CHI2 / _TRACK_NDOF;
    // cout<<" chi2dof   "<< chi2dof<<endl;
    if (_trk_type == 0)
    {
      _trks_chi2dof_vec.push_back(chi2dof);
      _trks_ghostprob_vec.push_back(_ghostprob);
      _trks_pt_vec.push_back(_TRACKS_PT);
      _trks_assoc_vec.push_back(_tracks_assoc_);
      _trks_type_vec.push_back(_tracks_TRACK_Type_);
      _processed_evts.push_back(_tracks_TRACK_Type_);
    }
    else
    {
      if ((int)(_tracks_TRACK_Type) == _trk_type)
      {
        _trks_chi2dof_vec.push_back(chi2dof);
        _trks_ghostprob_vec.push_back(_ghostprob);
        _trks_pt_vec.push_back(_TRACKS_PT);
        _trks_assoc_vec.push_back(_tracks_assoc_);
        _trks_type_vec.push_back(_tracks_TRACK_Type_);
        _processed_evts.push_back(_tracks_TRACK_Type_);
        _index++;
      }
      else
        _processed_evts.push_back(0);
    }
  }
  inFile_pre->Close();
  delete inFile_pre;

  //
  // calculate the new ghost prob
  //
  TFile *inFile = TFile::Open(_filelist.c_str());
  TTree *thetree = (TTree *)inFile->Get(_inputtree.c_str());
  std::vector<TMVA::Reader *> reader_vec(5);

  // TMVA reader init
  TMVA::Tools::Instance();
  if (_trk_type == 0)
  {
    for (int i = 0; i < 5; i++)
      reader_vec[i] = new TMVA::Reader(_reader_opt);
  }
  else
    reader = new TMVA::Reader(_reader_opt);

  // add variable to the fitting
  cout << "    -> Read variables into TMVA::Reader ..." << endl;
  if (_trk_type == 0)
  {
    for (int i = 0; i < 3; i++)
    { // 1, 3, 4
      // GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_obsVP");
      GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_FitVeloChi2");
      // GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_FitVeloNDoF");
    }

    for (int i = 1; i < 5; i++)
    { // 3, 5, 6
      if (i != 2)
      {
        // GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_obsFT");
        GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_FitTChi2");
        GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_FitTNDoF");
      }
    }
    /*
    for (int i = 1; i < 4; i++) 
    { // 3, 4, 5
      GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_obsUT");
    }
    */
    GPAddVariable(reader_vec[1], thetree, "UpgradeGhostInfo_FitMatchChi2");
    /*
    for (int i = 1; i < 4; i++) { // 3, 4, 5
      GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_UToutlier");
    }
    */
    for (int i = 0; i < 5; i++)
    { // 1, 3, 4, 5, 6
      // GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_veloHits");
      // GPAddVariable(reader_vec[i], thetree, "UpgradeGhostInfo_utHits");
      GPAddVariable(reader_vec[i], thetree, "TRACK_CHI2");
    }
    for (int i = 0; i < 5; i++)
    { // 1, 4, 6
      if (!((i == 1) || (i == 3)))
        GPAddVariable(reader_vec[i], thetree, "TRACK_NDOF");
    }

    for (int i = 1; i < 5; i++)
    { // 3, 4, 5, 6
      GPAddVariable(reader_vec[i], thetree, "TRACK_PT");
    }
    for (int i = 0; i < 5; i++)
    { // 1, 3, 4, 5, 6
      GPAddVariable(reader_vec[i], thetree, "TRACK_ETA");
    }

    // book MVA
    char name_index[500];
    for (int i = 0; i < 5; i++)
    {
      int _trk_tmp = const_num::NumToTRKType(i);

      sprintf(name_index, "dataset/weights/outputs/TMVA_%d_%s_%s_MLP.weights.xml", _trk_tmp, _dataset.c_str(), _varset.c_str());
      _weight_input = (string)name_index;

      reader_vec[i]->BookMVA(_implemention, _weight_input.c_str()); // FIXME
    }
  }
  else
  {
    
    if (_trk_type == 1 || _trk_type == 3 || _trk_type == 4)
    {
      // GPAddVariable(reader, thetree, "UpgradeGhostInfo_obsVP");
      GPAddVariable(reader, thetree, "UpgradeGhostInfo_FitVeloChi2");
      // GPAddVariable(reader, thetree, "UpgradeGhostInfo_FitVeloNDoF");
    }

    if (_trk_type == 3 || _trk_type == 5 || _trk_type == 6)
    {
      // GPAddVariable(reader, thetree, "UpgradeGhostInfo_obsFT");
      GPAddVariable(reader, thetree, "UpgradeGhostInfo_FitTChi2");
      GPAddVariable(reader, thetree, "UpgradeGhostInfo_FitTNDoF");
    }
    /*
    if (_trk_type == 3 || _trk_type == 4 || _trk_type == 5) {
      GPAddVariable(reader, thetree, "UpgradeGhostInfo_obsUT");
    }
    */
    if (_trk_type == 3)
    {
      GPAddVariable(reader, thetree, "UpgradeGhostInfo_FitMatchChi2");
    }
    /*
    if (_trk_type == 3 || _trk_type == 4 || _trk_type == 5) {
      GPAddVariable(reader, thetree, "UpgradeGhostInfo_UToutlier");
    }
    */
    // GPAddVariable(reader, thetree, "UpgradeGhostInfo_veloHits");
    // GPAddVariable(reader, thetree, "UpgradeGhostInfo_utHits");
    GPAddVariable(reader, thetree, "TRACK_CHI2");

    if (_trk_type == 1 || _trk_type == 4 || _trk_type == 6)
      GPAddVariable(reader, thetree, "TRACK_NDOF");

    if (_trk_type == 3 || _trk_type == 4 || _trk_type == 5 || _trk_type == 6)
      GPAddVariable(reader, thetree, "TRACK_PT");
    GPAddVariable(reader, thetree, "TRACK_NDOF");
    GPAddVariable(reader, thetree, "TRACK_ETA");

    // book MVA
    reader->BookMVA(_implemention, _weight_input.c_str()); // FIXME
  }

  //
  ntotal_evts = thetree->GetEntries();
  if (_run_evts > 0)
    ntotal_evts = _run_evts;
  _index = 0;

  clock_t tBeginTime = times(NULL);

  for (int ievt = 0; ievt < ntotal_evts; ievt++)
  {
    n_events++;
    if (n_events % 1000000 == 0)
      printf("Processing %2d.0 M events!\n", (n_events / 1000000));

    thetree->GetEntry(ievt);

    if (_trk_type == 0)
    {
      n_cuts++;
      int _num_tmp = const_num::TRKTypeToNum(_processed_evts[ievt]);

      Float_t new_ghostprob = reader_vec[_num_tmp]->EvaluateMVA(_implemention);
      Float_t netresponse = TabulatedFunction1D(new_ghostprob);
      // Float_t netresponse = TabulatedFunction1D(0.5 * ( 1. - new_ghostprob ));

      _new_ghost_vec.push_back(new_ghostprob);
      _flatten_ghost_vec.push_back(netresponse);
    }
    else
    {
      if (_processed_evts[ievt] == _trk_type)
      {
        n_cuts++;
        Float_t new_ghostprob = reader->EvaluateMVA(_implemention);
        Float_t netresponse = TabulatedFunction1D(new_ghostprob);
        // Float_t netresponse = TabulatedFunction1D(0.5 * ( 1. - new_ghostprob ));
        _new_ghost_vec.push_back(new_ghostprob);
        _flatten_ghost_vec.push_back(netresponse);
        _index++;
      }
    }
  }
  clock_t tEndTime = times(NULL);
  fCostTime = (double)(tEndTime - tBeginTime) / sysconf(_SC_CLK_TCK);

  inFile->Close();

  delete inFile;
  if (_trk_type != 0)
  {
    delete reader;
  }
  reader_vec.clear();
}

//
// last step, print out the information and save tree to root file
//
void GhostProb_eval::EndEvaluate()
{
  // The SlaveTerminate() function is called after all entries or objects
  // have been processed. When running with PROOF SlaveTerminate() is called
  // on each slave server.

  cout << "Processed "
       << n_events << " track candidates" << endl
       << n_cuts << " selected track candidates." << endl;

  printf("[times] TMVA Cost Time = %fSec\n", fCostTime);
  printf("[times] average Cost Time = %fSec\n", fCostTime / (1. * n_cuts));

  // create a new root file
  _output_root = new TFile(_outputfile.c_str(), "recreate");
  new_tree = new TTree("tracks", "tracks");

  new_tree->Branch("tracks_TRACK_CHI2NDOF", &_TRACK_CHI2NDOF, "tracks_TRACK_CHI2NDOF/F");
  new_tree->Branch("tracks_assoc", &_TRACK_assoc, "tracks_assoc/I");
  new_tree->Branch("ghostprob", &_TRACK_ghostprob, "ghostprob/F");
  new_tree->Branch("tracks_PT", &_TRACK_PT, "tracks_PT/F");
  new_tree->Branch("tracks_TRACK_Type", &_TRACK_Type, "tracks_TRACK_Type/I");
  new_tree->Branch("new_ghostprob", &_TRACK_newprob, "new_ghostprob/F");
  new_tree->Branch("flatten_ghostprob", &_TRACK_flattenprob, "flatten_ghostprob/F");

  // loop the vectors
  for (int i = 0; i < _new_ghost_vec.size(); i++)
  {
    _TRACK_CHI2NDOF = _trks_chi2dof_vec[i];
    _TRACK_assoc = _trks_assoc_vec[i];
    _TRACK_ghostprob = _trks_ghostprob_vec[i];
    _TRACK_newprob = _new_ghost_vec[i];
    _TRACK_flattenprob = 1 - _flatten_ghost_vec[i];

    _TRACK_PT = _trks_pt_vec[i];
    _TRACK_Type = _trks_type_vec[i];
    new_tree->Fill();
  }

  // write tree to root
  _output_root->WriteTObject(new_tree);
  _output_root->Close();
}

//
// deconstruction function
//
GhostProb_eval::~GhostProb_eval() {}

float GhostProb_eval::TabulatedFunction1D(const float x)
{

  if (x <= (*(m_xedges.begin())))
    return 0.f;
  if (x >= (*(m_xedges.end() - 1)))
    return 1.f;

  // iterator to the first element that is not smaller than x
  // may be end() - if x is larger than the last element
  // may be begin() - if x is smaller than the first element
  auto up = std::lower_bound(m_xedges.begin(), m_xedges.end() - 1, x);
  // iterator to the last element that is smaller than x
  // (may be out of range)
  auto low = up - 1;

  // y-value for the lower edge of the x-bin we're in
  float edge = m_width * (low - m_xedges.begin());

  // by what fraction did we enter the x bin
  float add = m_width * (x - (*low)) / ((*up) - (*low));

  return edge + add;
}
