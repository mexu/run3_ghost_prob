//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov  3 20:21:52 2015 by ROOT version 5.34/32
// from TTree tracks/tracks
// found on file: /afs/cern.ch/user/h/hyin/storage/workspace/Skimmed_samples/GhostPorb/1.6/myalg2.root
//////////////////////////////////////////////////////////

#ifndef GhostProb_eval_h
#define GhostProb_eval_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TH1.h>
#include <TTree.h>
#include <TROOT.h>
#include <TSystem.h>

#include "TMVA/MethodCategory.h"
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"

#include "histo_Handle.hpp"
#include "PParameterReader.hpp"
#include "const_numbers.hpp"

#include <iostream>
#include <string>
#include <cstdlib>
#include <map>
#include "/home/dqliu/work/ghost_service_work/second/run3_ghost_prob-master/Sigmoid2_1/Rich1DTabFunc.h"
// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.
using namespace std;
using namespace const_num;

class GhostProb_eval{
 private:
  TString _parameter_filename;
  int _trk_type;
  int _run_evts;
  void ReadParameters();
  void ReCal_TMVA();
  void EndEvaluate();
  // evaluation
  void GPAddVariable(TMVA::Reader* red, TTree* tree, std::string varname);
  float TabulatedFunction1D(const float x);
  string _filelist;
  string _inputtree;
  string _outputfile;
  string _EvtType;
  string _varset;
  string _dataset;
  string _reader_opt;
  string _weight_input;
  string _flat_input;
  string _implemention;

  int n_events;
  int n_cuts;
  double fCostTime;

  // TMVA fitting
  TMVA::Reader* reader;

  TFile* _output_root;
  TTree* new_tree;

  Float_t _TRACK_CHI2;
  Float_t _TRACK_NDOF;
  Float_t _tracks_assoc;
  Float_t _ghostprob;
  Float_t _TRACKS_PT;
  Float_t _tracks_TRACK_Type;

  Float_t _TRACK_CHI2NDOF;
  int _TRACK_assoc;
  Float_t _TRACK_ghostprob;
  Float_t _TRACK_newprob;
  Float_t _TRACK_flattenprob;
  Float_t _TRACK_PT;
  int _TRACK_Type;

  int ntotal_evts;
  vector<Float_t> _trks_chi2dof_vec;
  vector<int> _trks_assoc_vec;
  vector<Float_t> _trks_ghostprob_vec;
  vector<int> _trks_type_vec;
  vector<Float_t> _trks_pt_vec;
  vector<Float_t> _new_ghost_vec;
  vector<Float_t> _flatten_ghost_vec;
  vector<int> _processed_evts; 
  vector<float> m_xedges;
  float m_width;
 public:
  GhostProb_eval(const char *s, int type, int nevts);
  ~GhostProb_eval();
};

#endif
