# run3_ghost_prob



## Install stack 
```
curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - tuples
cd tuples
lhcb-proxy-init
kinit -r 7d user@CERN.CH
make BINARY_TAG=x86_64_v2-centos7-gcc12+detdesc-opt Moore

``` 
Copy new `./option_files/UpgradeGhostId.cpp` to  `Rec/Tr/TrackTools/src/UpgradeGhostId.cpp`, `./option_files/UpgradeGhostIdNT.cpp` to `Rec/Tr/TrackMCTools/src/UpgradeGhostIdNT.cpp` and `./option_files/TrackAddNNGhostId.cpp` to `Rec/Tr/TrackUtils/src/TrackAddNNGhostId.cpp 
Add one line `src/TrackAddNNGhostId.cpp` at `Rec/Tr/TrackUtils/CMakeLists.txt'

then  make again

``` 
make BINARY_TAG=x86_64_v2-centos7-gcc12+detdesc-opt Moore
```

# Tuple
At `Moore/Hlt/RecoConf/python/RecoConf/mc_checking.py`, adding following lines before `def get_item(x, key):` 

```
from RecoConf.hlt2_tracking import get_UpgradeGhostId_tool_no_UT
from PyConf.Tools import UpgradeGhostIdNT

def get_upgradeghostid_tuple(get_linker, get_ghosttool=get_UpgradeGhostId_tool_no_UT, mcps = mc_unpackers()["MCParticles"]):
    return UpgradeGhostIdNT(Tool=get_ghosttool(), InputTrackLinks = get_linker, MCParticles = mcps)

from PyConf.Algorithms import TrackAddNNGhostId
def make_ghost_prob_tuple(name, tracks, upgradeGP):
   return TrackAddNNGhostId( name = name, inputLocation = tracks, GhostIdTool = upgradeGP)
```
The actually main function is `get_fitted_tracks_checkers` in `mc_checking.py`, change it to:
```
@configurable
def get_fitted_tracks_checkers(
        FittedTracks,
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_tracking_system,
        with_mc_hits=True,
        fitted_track_types=["BestLong", "BestDownstream", "BestUpstream"],
        max_ghost_prob=0.5,
        with_UT=True):

    links_to_lhcbids = make_links_lhcbids_mcparticles()

    links_to_tracks = {}
    for track_type in fitted_track_types:
        links_to_tracks[track_type] = make_links_tracks_mcparticles(
            InputTracks=FittedTracks[track_type],
            LinksToLHCbIDs=links_to_lhcbids)

    categories = []

    if "BestLong" in fitted_track_types:
        good_long_tracks = make_track_filter(
            InputTracks=FittedTracks["BestLong"],
            code=(F.GHOSTPROB < max_ghost_prob))
        categories.append((FittedTracks["BestLong"], "BestLong", "BestLong",
                           "BestLong"))
        categories.append((good_long_tracks, "LongGhostFiltered", "BestLong",
                           "BestLong"))

    if "BestDownstream" in fitted_track_types:
        good_downstream_tracks = make_track_filter(
            InputTracks=FittedTracks["BestDownstream"],
            code=(F.GHOSTPROB < max_ghost_prob))
        categories.append((FittedTracks["BestDownstream"], "BestDownstream",
                           "BestDownstream", "BestDownstream"))
        categories.append((good_downstream_tracks, "DownstreamGhostFiltered",
                           "BestDownstream", "BestDownstream"))

    if "BestUpstream" in fitted_track_types:
        categories.append((FittedTracks["BestUpstream"], "BestUpstream",
                           "BestUpstream", "BestUpstream"))
        
    efficiency_checkers = []

    for tracks, tr_key, mc_key, hit_key in categories:
        checker = check_tracking_efficiency(
            InputTracks=tracks,
            TrackType=tr_key,
            LinksToTracks=links_to_tracks[mc_key],
            LinksToLHCbIDs=links_to_lhcbids,
            MCCategories=get_mc_categories(mc_key),
            HitTypesToCheck=get_hit_type_mask(hit_key),
        )
        efficiency_checkers.append(checker)
        
        if tr_key == "BestLong":
            ghost_prob = make_ghost_prob_tuple(name=tr_key, tracks=tracks["v1"], upgradeGP=get_upgradeghostid_tuple(get_linker = links_to_tracks[tr_key]))
            efficiency_checkers.append(ghost_prob)

    if with_mc_hits:
        if with_UT:
            for tr_type in fitted_track_types:
                resolution_checker = check_track_resolution(
                    FittedTracks[tr_type], suffix=tr_type)
                efficiency_checkers.append(resolution_checker)
        else:
            for tr_type in fitted_track_types:
                resolution_checker = check_track_resolution(
                    FittedTracks[tr_type],
                    suffix=tr_type,
                    make_links_lhcbids_mcparticles=
                    make_links_lhcbids_mcparticles)
                efficiency_checkers.append(resolution_checker)

    return efficiency_checkers
```

```
./Moore/build.x86_64_v2-centos7-gcc12+detdesc-opt/run gaudirun.py option_files/skim.qmt
```
## MVA trainning
Achieved by Sigmoid directory
